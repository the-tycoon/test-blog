<?php
/**
 * The Post Model
 * 
 * @author Abhay Chauhan
 */

namespace Models;
class Post extends Shared\Model{

      /**
       * @column
       * @readwrite
       * @type text
       * @length 400
       */
      protected $_title;

      /**
       * @column
       * @readwrite
       * @type text
       */
      protected $_content;

      /**
       * @column
       * @readwrite
       * @type integer
       */
      protected $_authorid;

}