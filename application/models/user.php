<?php

/**
 * The User Model
 *
 * @author Abhay Chauhan
 */
namespace Models;
class User extends Shared\Model {

    /**
     * @column
     * @readwrite
     * @type text
     * @length 100
     *
     */
    protected $_first;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 100
     */
    protected $_last;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 250
     */
    protected $_email;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 100
     */
    protected $_password;

}
