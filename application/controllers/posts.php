<?php
/**
 * Posts controller 
 * 
 * @author Abhay Chauhan
 */
use Shared\Controller as Controller;
use Framework\RequestMethods as RequestMethods;

class Posts extends Controller{

      public function create(){
            if(RequestMethods::post("submit")){
                  $title = RequestMethods::post("title");
                  $content = RequestMethods::post("content");

                  $view = $this->getActionView();
                  $error = false;

                  if(empty($title)){
                        $view->set("title_error", "Title Required.");
                        $error = true;
                  }

                  if(empty($content)){
                        $view->set("content_error", "Content Required");
                        $error = true;
                  }

                  if(!$error){
                       $post = new Models\Post(array(
                              "title" => $title,
                              "content" => $content
                       ));

                       $post->save();
                       $view->set("success", true);
                  }
            }
      }

      public function view(){
            $view = $this->getActionView();
            
            if(RequestMethods::get("id")){
                  $id = RequestMethods::get("id");

                  $post = Post::first(array(
                        "id = ?" => $id,
                        "live = ?" => true
                  ));

                  if(!empty($post)){
                        $view
                              ->set("post_title", $post->getTitle())
                              ->set("post_content", $post->getContent())
                              ->set("success", true);
                  }else{
                        $view
                              ->set("success", false)
                              ->set("found_error", "Post not found.");
                  }
            }
      }

}