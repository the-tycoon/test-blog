<?php
/**
 * Register controller 
 * 
 * @author Abhay Chauhan
 */
use Shared\Controller as Controller;

class User extends Controller{

      public function register(){
            if(RequestMethods::post("register")){
                  $first = RequestMethods::post("first");
                  $last = RequestMethods::post("last");
                  $email = RequestMethods::post("email");
                  $password = RequestMethods::post("password");
            
                  $view = $this->getActionView();
                  $error = false;

                  if(empty($first)){
                        $view->set("first_error", "First name cannot be left blank.");
                        $error = true;                        
                  }

                  if(empty($second)){
                        $view->set("last_error", "Last name cannot be left blank.");
                        $error = true;
                  }

                  if(empty($email)){
                        $view->set("email_error", "Email cannot be left blank.");
                        $error = true;
                  }

                  if(empty($password)){
                        $view->set("password_error", "Password cannot be left blank.");
                        $error = true;
                  }

                  if(!$error){
                        $user = new User(array(
                              "first" => $first,
                              "last" => $last,
                              "email" => $email,
                              "password" => $password
                        ));

                        $user->save();
                        $view->set("success", true);
                  }
            
            }
            
      }

      public function login(){
            if(RequestMethods::post("login")){
                  $email = RequestMethods::post("email");
                  $password = RequestMethods::post("password");
                  
                  $view = $this->getActionView();
                  $error = false;

                  if(empty($email)){
                        $view->set("email_error", "Email cannot be left blank.");
                        $error = true;
                  }

                  if(empty($password)){
                        $view->set("password_error", "Password cannot be left blank.");
                        $error = true;
                  }

                  if(!$error){
                        $user = User::first(array(
                              "email" => $email,
                              "password" => $password,
                              "live" => true,
                              "deleted" => false
                        ));

                        if(!empty($user)){
                              echo "User Logged In";
                        }else{
                              echo "Error";
                        }
                  }

            }
      }

}